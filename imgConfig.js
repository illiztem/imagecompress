// Adjust the quality between 0 (worst) and 100 (best)
// Output format, can be 'jpg', 'jpeg', 'png', 'webp', 'tiff', 'gif', 'avif'

const imgConfig = {
    compressionQuality: 80,
    outputFormat: 'jpg'
}

module.exports = imgConfig