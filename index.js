const sharp = require('sharp')
const fs = require('fs')
const { compressionQuality, outputFormat } = require('./imgConfig')

const inputFolder = './'
const outputFolder = 'output'

if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder)
}

const files = fs.readdirSync(inputFolder)
if (files.length === 0) {
    console.log(`No files found in the folder: ${inputFolder}`)
    process.exit(1)
}

// Find the first image file in the folder (jpg, jpeg, png, gif, webp)
const imageFile = files.find(file => /\.(jpg|jpeg|png|gif|webp)$/i.test(file))

if (!imageFile) {
    console.log(`No image files found in the folder: ${inputFolder}`)
    process.exit(1)
}

const supportedFormats = ['jpg', 'jpeg', 'png', 'webp', 'tiff', 'gif', 'avif']
const isValidFormat = supportedFormats.includes(outputFormat.toLowerCase())

if (!isValidFormat) {
    console.error(`Invalid output format specified: ${outputFormat}`)
    process.exit(1)
}

const inputImagePath = `${inputFolder}/${imageFile}`
const outputImagePath = `${outputFolder}/${imageFile.replace(/\.[^/.]+$/, '')}.${outputFormat}`

sharp(inputImagePath)
    .toFormat(outputFormat, { quality: compressionQuality })
    .toFile(outputImagePath, (err, info) => {
        if (err) {
            console.error(err)
        } else {
            console.log(`Image compressed and format changed successfully. Output: ${outputImagePath}`)
            console.log(info)
        }
    })