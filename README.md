# imageCompress
> Information is always the most precious asset in any format, do not give it away so easily.

**imageCompress** is a simple local image compressor and file converter that prevents uploading your files into one of those "~~free~~ online compressor or file converters".

## Installation
Use the node package manager (npm) to install required dependences.

```bash
npm install
```

## Usage
1. Put the image you want to compress or convert into the root folder.
2. Change the value of *compressionQuality* and *outputFormat* in ***imgConfig.js*** to your desired output configurations and save the file.
3. Run the start command script on CLI:
```bash
npm start
```
4. Done! Your compressed/converted image will be on *output* folder.

## License
[MIT](https://choosealicense.com/licenses/mit/)